const express = require('express');
const fs = require("fs");

const app = express();

app.get('/api/users', function (req, res) {
  fs.readFile("db/users.json", "utf-8", (error, data) => {
    const users = JSON.parse(data);
    res.status(200).send({
      success: 'true',
      users: user
    });
  });
});


app.get('/api/users/:id', function (req, res) {
  fs.readFile("db/users.json", "utf-8", (error, data) => {
    const users = JSON.parse(data);
   
    const user = users.filter(
      user => user.id === req.params.id
    );

    if(user.length > 0) {
      res.status(200).send({
        success: 'true',
        user: user
      });
    } else {
      res.status(200).send({
        success: 'true',
        message: 'Usuario no encontrado'
      });
    }

  });
});

app.listen(1234, function () {
  console.log(`API corriendo`);
});